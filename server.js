'use strict';

const express = require('express');
const opentracing = require('opentracing');

// NOTE: the default OpenTracing tracer does not record any tracing information.
// Replace this line with the tracer implementation of your choice.
const tracer = new opentracing.Tracer();

const span = tracer.startSpan('http_request');

// Constants
const PORT = 6702;
const HOST = 'localhost';

// App
const app = express();
app.get('/', (req, res) => {
  span.log({ 'req.baseUrl': req.baseUrl });
  console.debug(req.headers);
  res.send('Hello from Node Service 2');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

const consulHelper = require('ijooz-consul');
consulHelper.registerService({ name: 'NodeService2', port: PORT });
